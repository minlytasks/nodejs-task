const express = require('express');
const app = express();
const multer = require("multer");
const path = require("path");
const dirTree = require("directory-tree");
const tree = dirTree('./upload/images/');

// storage engine
const storage = multer.diskStorage({
    destination: './upload/images',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
    }
})

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1000000
    }
})

app.use('/photo', express.static('upload/images'));
app.post("/upload", upload.single('photo'), (req, res) => {
    res.json({
        photo_url: `http://localhost:4000/photo/${req.file.filename}`
    })
})

app.get('/photos', function (_req, res) {
    res.json(tree);
});

function errHandler(err, req, res, next) {
    if (err instanceof multer.MulterError) {
        res.json({
            success: 0,
            message: err.message
        })
    }
}
app.use(errHandler);

app.listen(4000, () => {
    console.log("server up and running");
})